
#ifndef __COUNTER_H__
#define __COUNTER_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct
{
	bool enabled;
	uint32_t value;
} Counter;

Counter Counter_create(
	const bool enabled,
	const uint32_t value);

void Counter_setValue(
	Counter* const counter,
	const uint32_t value);

void Counter_enable(
	Counter* const counter);

void Counter_disable(
	Counter* const counter);

bool Counter_isEnabled(
	const Counter* const counter);

uint32_t Counter_getValue(
	const Counter* const counter);

#endif // __COUNTER_H__
