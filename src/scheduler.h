
#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include "./counter.h"

#include <stdbool.h>  // For bool

bool Scheduler_run(
	Counter* const counter);

#endif // __SCHEDULER_H__
