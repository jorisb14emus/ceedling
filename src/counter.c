
#include "./counter.h"

#include <assert.h>
#include <memory.h>

Counter Counter_create(
	const bool enabled,
	const uint32_t value)
{
	Counter counter;
	memset(&counter, 0, sizeof(Counter));
	counter.enabled = enabled;
	counter.value = value;
	return counter;
}

void Counter_setValue(
	Counter* const counter,
	const uint32_t value)
{
	assert(counter != NULL);
	counter->value = value;
}

void Counter_enable(
	Counter* const counter)
{
	assert(counter != NULL);
	counter->enabled = true;
}

void Counter_disable(
	Counter* const counter)
{
	assert(counter != NULL);
	counter->enabled = false;
}

bool Counter_isEnabled(
	const Counter* const counter)
{
	assert(counter != NULL);
	return counter->enabled;
}

uint32_t Counter_getValue(
	const Counter* const counter)
{
	assert(counter != NULL);
	return counter->value;
}
