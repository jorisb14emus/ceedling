
#include "./scheduler.h"

#include <stdio.h>

bool Scheduler_run(
	Counter* const counter)
{
	fprintf(stdout, "Checking counter...\n");

	if (!Counter_isEnabled(counter))
	{
		fprintf(stdout, "Disabled counter...\n");
		return false;
	}

	fprintf(stdout, "Starting scheduler...\n");

	for (;;)
	{
		const uint32_t value = Counter_getValue(counter);

		if (value >= 10)
		{
			fprintf(stdout, "Reached the limit. Exiting loop...\n");
			break;
		}

		Counter_setValue(counter, value + 1);
	}

	fprintf(stdout, "Exiting scheduler...\n");
	return true;
}
