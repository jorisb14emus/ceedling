
#ifndef TEST
#	define TEST
#endif

#ifdef TEST

#include "unity.h"

#include "counter.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_counter_NeedToImplement(void)
{
	TEST_IGNORE_MESSAGE("Need to Implement counter");
}

#endif // TEST
