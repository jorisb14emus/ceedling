
/**
 * @warning IS NOT WORKING!
 */

#include "./unity_helper.h"

void Helper_assertEqualCounter(
	Counter expected,
	Counter actual,
	const unsigned short line)
{
	UNITY_TEST_ASSERT_EQUAL_UINT8(expected.enabled, actual.enabled, line, "Counter Struct Failed For Field enabled");
	UNITY_TEST_ASSERT_EQUAL_UINT32(expected.value, actual.value, line, "Counter Struct Failed For Field value");
}
