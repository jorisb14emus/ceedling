
/**
 * @warning IS NOT WORKING!
 */

#ifndef __UNITY_HELPER_H__
#define __UNITY_HELPER_H__

#include "counter.h"

#include "unity.h"

void Helper_assertEqualCounter(
	Counter expected,
	Counter actual,
	const unsigned short line);

#define UNITY_TEST_ASSERT_EQUAL_Counter(expected, actual, line) \
	Helper_assertEqualCounter(expected, actual, line)

#define TEST_ASSERT_EQUAL_Counter(expected, actual) \
	UNITY_TEST_ASSERT_EQUAL_Counter(expected, actual, __LINE__)

#endif
