
#ifndef TEST
#	define TEST
#endif

#ifdef TEST

#include "unity.h"

#include "cmock.h"
#include "mock_counter.h"

#include "scheduler.h"

#include <memory.h>

Counter counter;

void setUp(
	void)
{
	mock_counter_Init();
	mock_counter_Verify();
	memset(&counter, 0, sizeof(Counter));
}

void tearDown(
	void)
{
	mock_counter_Destroy();
}

void test_whenCounterIsDisabled_thenSchedulerShouldExitWithFALSEretval(
	void)
{
	counter.enabled = false;
	const bool expectedValue = false;

	Counter_isEnabled_ExpectAndReturn(
		&counter, expectedValue);

	const bool actualValue = Scheduler_run(&counter);
	TEST_ASSERT_EQUAL_UINT8(expectedValue, actualValue);
}

void test_whenCounterIsEnabled_thenSchedulerShouldExitWithTRUEretval(
	void)
{
	counter.enabled = true;
	const bool expectedValue = true;

	Counter_isEnabled_ExpectAndReturn(
		&counter, expectedValue);

	for (uint32_t i = 0; i < 11; ++i)
	{
		Counter_getValue_ExpectAndReturn(
			&counter, i);

		Counter_setValue_Expect(
			&counter, i + 1);
	}

	const bool actualValue = Scheduler_run(&counter);
	TEST_ASSERT_EQUAL_UINT8(expectedValue, actualValue);
}

#endif // TEST
